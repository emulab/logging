#!/bin/bash
cd /local/logdata
sudo chmod g+w .
mkdir loglizer
git clone https://github.com/logpai/loglizer.git loglizer/
sudo chmod -R g+w .
sudo apt-get -y update
sudo apt install -y python-sklearn  
sudo apt-get install -y python-numpy python-scipy
sudo apt-get install -y python-pandas
# For DataParser
sudo apt-get install -y python3-elasticsearch python3-certifi python3-git
