# Repository emulab/logging

Configuration and code for collecting and processing logfiles for Emulab,
CloudLab, and related testbeds.

## Directory structure

* `config` - configuration files
    * `logstash` - configuration for the logstash server
    * `filebeat` - configuration for the filebeat agents that run on each server
    * `nginx` - configuration for the nginx reverse proxy that runs in front of kibana
* `ansible` - ansible playbooks and inventory for setting up and maintaining the logging infrastructure
* `patches` - miscellaneous patches that need to be applied

## Creating CA infrastructure for logbeats

In order to use logbeats securely, we need to generate SSL keys to use on the
server and the client. This only needs to be done once per installation, and
can be done on any machine, but we recommend doing it on the host running
`logstash`.

Install the `easy-rsa` package:

* `sudo apt-get install easy-rsa`

Copy the `easyrsa` directory somewhere else (so that it doesn't get clobbered if
the package is updated), and run all further commands there:

* `sudo cp -r /usr/share/easy-rsa /var/easy-rsa`
* `cd /var/easy-rsa`
* `sudo chown -R $USER .`

Edit the `vars` file in this directory to edit the stuff in the `KEY\_COUNTRY`
and below. This stuff is not super important, but its nice for it to not look
too bogus.

Copy the `openssl.conf` file into place:

* `sudo cp openssl-1.0.0.cnf openssl.cnf`

Source the `vars` file to get all of the variables you just edited into your
shell, and get started building the CA:

* `source vars`
* `./clean-all`
* `./build-ca`:

Make a key for the server; you can leave the challenge password empty:

* `./build-key-server logstash.emulab.net`

Make a key for each machine that will be a source for logfiles (eg. will
run `filebeat`): (again, you may leave the challenge password empty)

* `./build-key boss.utah.cloudlab.us`

## Setting up log sources (ie. testbed servers)

The file `ansible/loghosts` contains a list of all hosts (specifically under
the `[logsources]` section) that will run `filebeat` to send files to the
logstash host. These are manged with Ansible, which needs to be installed
on the local host, but not on the servers.

The config file for `filebeat` can be found in
`config/filebeat/filebeat-common.yml`. Using the `run-filebeats.yml` playbook
(documented below), this gets synced out to all log source servers. At the present
time, the same config file is used for all source hosts; we may support per-host
config files in the future.

To get `filebeat` installed on all hosts (in `/usr/testbed/go`), run:

* `cd ansible`
* `ansible-playbook -i loghosts install-filebeats.yml`

To (re)start the `filebeat` processes on all servers:

* `cd ansible`
* `ansible-playbook -i loghosts run-filebeats.yml`

*Note* that the above does not set up `filebeat` to restart if it dies or the
machine it is running on reboots.

## Installing Elasticsearch

Assuming Ubuntu (or another Debian-based distribution):

Add the Elastic APT repository to the system (full instructions at https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html):

* `wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add`
* `sudo apt-get install apt-transport-https`
* `echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list`
* `sudo apt-get update`

Install `elasticsearch` itself (and the JRE it depends on):

* `sudo apt-get install openjdk-8-jre-headless elasticsearch`

Start it and set it to start on reboot:

* `sudo systemctl enable elasticsearch`
* `sudo systemctl start elasticsearch`

The default configuration for elasticsearch should be sufficient; it only
listens on `locahost`. If you need to edit the configuration (eg. to change
where it stores things on disk), the configuration is in `/etc/elasticsearch`

## Installing Logstash

Set up the elastic APT repository as above if you have not already done so.

Install `logstash` itself:

* `sudo apt-get install openjdk-8-jre-headless logstash`

Copy the appropriate configuration file from `config/logstash/` to
`/etc/logstash/conf.d/`

Start it and set it to start on reboot:

* `sudo systemctl enable logstash`
* `sudo systemctl start logstash`

## Installing Kibana

Kibana has no built-in user authentication (unless you pay), so we run it
behind an `nginx` reverse proxy, where we can set up HTTP Simple Auth. 

Install `nginx`:

* `sudo apt-get install nginx`
* `sudo systemctl enable nginx`
* `sudo systemctl start nginx`

### Get a Let's Encrypt SSL certificate

Get an https certificate from LetsEncrypt (adapted from this page: https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-18-04 )

* `sudo add-apt-repository ppa:certbot/certbot`
* `sudo apt-get install python-certbot-nginx`
* `sudo certbot --nginx -d kibana.emulab.net`

The above steps will all ask for a bit of confirmation information, but it
should all be straighforward. (Note that the hostname in the third step might
be different.) If `certbot` asks whether it should redirect all `http` traffic
to `https`, say yes. 

You should now be able to visit your site (eg. http://kibana.emulab.net/), and
get redirected to `https` and get an nginx default page.

### Installing the `nginx` configuration file

Now, copy the appropriate config file from `config/nginx` to
`/etc/nginx/sites-available`. Go to `/etc/nginx/sites-enabled`, remove the
link to `default`, and symlink the file you just copied into `sites-avaiable`
here. Restart `nginx` with `sudo systemctl restart nginx`

Visiting the site should now give you a popup dialog asking for a username and
password.

### Setting up users

**Important: Make sure you use strong (random) passwords!**

Install the `htpassword` command:

* `sudo apt-get install apache2-utils`

Create the `.htpassword` file:

* `sudo touch /etc/nginx/.htpasswd`

Create a user:

* `sudo htpasswd /etc/nginx/.htpasswd username`

### Install and start kibana

* `sudo apt-get install kibana`
* `sudo systemctl enable kibana`
* `sudo systemctl start kibana`
