#!/bin/sh

LD=/local/logdata/loglizer
PD=/local/repository/loglizer-patches

cd $LD && git apply $PD/loglizer.patch
cp $PD/Invariants_without_labels.py $LD/demo/Invariants_without_labels.py
