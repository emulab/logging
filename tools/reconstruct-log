#!/usr/bin/env python3

import csv
import logging
import re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--session", help="Limit to the specified session", nargs='*')
parser.add_argument("--eventids", help="Include the following list of eventids", nargs='*')
parser.add_argument("--highlight", help="Highlight eventids instead of filtering", nargs='?', const=True)
parser.add_argument("--verbose", help="Print debugging messages", nargs='?', const=True)
parser.add_argument("--file", help=".csv file with messages",default="data.csv")
parser.add_argument("--include-sessionid", help="Include sessionid in the log output", nargs='?', const=True)
parser.add_argument("-n", help="Don't print out the reconstructed log", nargs='?', const=True)
args = parser.parse_args()

months = { 'Jan' : 1, 'Feb' : 2, 'Mar' : 3, 'Apr' : 4, 'May' : 5, 'Jun' : 6, 'Jul' : 7, 'Aug' : 8, 'Sep' : 9, 'Oct' : 10, 'Nov' : 11, 'Dec' : 12 }

def sortcsvkey(row):
    return(sortkey(row[0]))

def sortkey(line):
    matches = re.match('(\w+)\s+(\d+)\s+(\d+):(\d+):(\d+)', line)
    return(months[matches.group(1)]*10**8 + int(matches.group(2))*10**6 + int(matches.group(3))*10**4 + int(matches.group(4))*10**2 + int(matches.group(5))*10**0)

def cleanup(line, altmode):
    if not altmode:
        return(re.sub(r'\s*blk_\d+\s*', '',line))
    else:
        return(re.sub(r'\s*blk_(\d+)\s*', r' (S \1)',line))

# Default to logging to stdout
logging.basicConfig(level=(logging.DEBUG if args.verbose else logging.ERROR), format='%(levelname)s - %(message)s')

lines = []
filename = args.file

with open(filename, newline='') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        lines.append(row)

# Remove the header line
lines.pop(0)

logging.info("Loaded {} lines from {}".format(len(lines), filename))

logging.debug("Message {} has sort key {}".format(lines[0][0],sortcsvkey(lines[0])))

if args.session:
    lines = list(filter(lambda x: any(s in x[0] for s in args.session), lines))
    logging.info("Filtered down to {} lines for session(s) {}".format(len(lines),args.session))

if args.eventids and not args.highlight:
    lines = list(filter(lambda x: x[1] in args.eventids, lines))
    logging.info("Filtered down to {} lines for eventids {}".format(len(lines),args.eventids))

lines.sort(key=sortcsvkey) 

logging.info("Sorted all lines")

if not args.n:
    for row in lines:
        if args.highlight:
            if row[1] in args.eventids:
                print("*** ",end='')
            else:
                print("    ",end='')
        print(",".join(row) if args.verbose else cleanup(row[0], args.include_sessionid))
