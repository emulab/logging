import pandas as pd
import numpy as np
import sys
from sklearn import svm
from Models.featurize import featurize as make_features
from sklearn.model_selection import KFold
from sklearn import metrics
import re

class invariant_clf:
	def __init__(self):
		self.invariants = None
		self.data = None

	def load_invariants(self, file):
		f = open(file, 'r') 
		contents = f.read()

		inv_list = []
		event_ids = re.findall(r'\((.*?)\)', contents)
		inv_counts = re.findall(r'\[(.*?)\]', contents)
		
		for i in range(0,len(event_ids)):
			event_id_list = event_ids[i].split(',')
			inv_counts_list = inv_counts[i].split(',')

			invariant = {}
			for j in range(0,len(event_id_list)):
				event_id = int(event_id_list[j].replace(' ',''))
				inv_count = float(inv_counts_list[j].replace(' ',''))
				invariant[event_id] = inv_count
			print(invariant)
			inv_list.append(invariant)
		self.invariants = inv_list

	def eval_inv(self, df):
		pred = []
		for index, row in df.iterrows():
			label = 1
			for inv in self.invariants:
				count = 0
				for event_id in inv:
					#print(inv[event_id],row[str(event_id)])
					count += (inv[event_id] * row[str(event_id)])
					#print(count)
				if count != 0:
					label = -1
			pred.append(label)
		return pred