import pandas as pd
import numpy as np
from sklearn import linear_model
from Models.featurize import featurize as make_features
from sklearn.model_selection import KFold
from sklearn import metrics

class Logistic:
	def __init__(self):
		self.raw = None
		self.data = None
		self.clf = None
		print("Log initializing")

	def test(self, x_data, y_data):
		print("Testing model")
		#x_data, y_data = make_features(test)
		y_pred = self.clf.predict(x_data)

		tn, fp, fn, tp = metrics.confusion_matrix(y_data,y_pred).ravel()
		print("TN: ", tn, "FP: ",fp)
		print("FN: ",fn, "TP: ", tp)
		print("Precision:",metrics.precision_score(y_data, y_pred))
		print("Recall:",metrics.recall_score(y_data, y_pred))
		print("F1 Score:",metrics.f1_score(y_data, y_pred))

		return y_pred


	def train(self, x_df, y_df):
		x_list = []
		for index, row in x_df.iterrows(): 
			x_list.append(list(row))
		x_data= np.array(x_list)
		y_data = np.array(y_df)

		# Select best hyperparamters
		penalties = ['l1', 'l2']
		c_vals = [0.1, 1, 10, 100]
		classWeights = [None, 'balanced']
		solvers = ['liblinear']
		num_iter = [1000, 10000, 100000]
		
		folds = 5
		bestF = 0
		# unigram
		bestParams = ['l1', 10, 'balanced', 'liblinear', 100]
		# bigram
		#bestParams = ['l1', 1, None, 'liblinear', 1000]
		#partials
		#bestParams = ['l1', 0.1, None, 'liblinear', 1000]
		print("Selecting hyperparamters")
		for penalty in penalties:
			# Uncomment to skip cross validation
			# continue
			for weight in classWeights:
				for s in solvers:
					for n in num_iter:
						for c in c_vals:
							F = self.crossValidate(folds, x_data, y_data, penalty, c, weight,s,n)
							print(penalty, c, weight, s, n)
							print("Avg: ", F)
							print("")
							if F > bestF:
								bestF = F
								bestParams = [penalty, c, weight, s, n]
							if penalty == 'none':
								break
		print("Best: ", bestF)
		print(bestParams)

		print("Training final model")
		penalty = bestParams[0]
		c =bestParams[1]
		weight = bestParams[2]
		s = bestParams[3]
		n = bestParams[4]
		self.clf = linear_model.LogisticRegression(penalty = penalty, C = c, class_weight = weight, solver = s, max_iter = n)
		self.clf.fit(x_data, y_data)

	def subsetData(self, x, y, N):
		negFound = False
		for i in range(0,N):
			if y[i] == -1:
				negFound = True
		if negFound:
			x_set = x[0:N]
			y_set = y[0:N]
		else:
			for i in range(0,len(y)):
				if y[i] == -1:
					x_set = x[i-N:i]
					y_set = y[i-N:i]
		return x_set, y_set

	def crossValidate(self, folds, x_data, y_data, pentaly, c, weight, s,n):
		# Cross validation
		total_f = 0.0
		kf = KFold(n_splits = folds)
		for train_index, test_index in kf.split(x_data):
			X_train, X_test = x_data[train_index], x_data[test_index]
			y_train, y_test = y_data[train_index], y_data[test_index]
			#if -1 not in y_train:
			#	continue
			clf = linear_model.LogisticRegression(penalty = pentaly, C = c, class_weight = weight, solver = s, max_iter = n)
			clf.fit(X_train, y_train)
			y_pred = clf.predict(X_test)

			# tn, fp, fn, tp = metrics.confusion_matrix(y_test,y_pred).ravel()
			# print("TN: ", tn, "FP: ",fp)
			# print("FN: ",fn, "TP: ", tp)
			# print("Precision:",metrics.precision_score(y_test, y_pred))
			# print("Recall:",metrics.recall_score(y_test, y_pred))
			# print("F1 Score:",metrics.f1_score(y_test, y_pred))
			

			total_f += metrics.f1_score(y_test, y_pred)
		return total_f/folds