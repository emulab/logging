from generation.session_example import example
import numpy as np
def featurize(data):
	# Collect all event_ids
	event_ids = []
	for ex in data:
		for event in ex.sequence:
			if event not in event_ids:
				event_ids.append(str(event))
	event_ids = sorted(event_ids)
	print(event_ids)
	print(len(event_ids))

	blankFeatureDict = {}
	for event1 in event_ids:
		blankFeatureDict[event1] = 0

		# for event2 in event_ids:
		# 	combinedEvent = event1 + event2
		# 	blankFeatureDict[combinedEvent] = 0

	featureVectors = []
	labels = []
	for ex in data:
		featureDict = blankFeatureDict.copy()
		for i in range(0,len(ex.sequence)):
			current = ex.sequence[i]
			if i > 0:
				previous = ex.sequence[i-1]
			else:
				previous = None

			featureDict[current] += 1

			# if previous != None:
			# 	combinedEvent = str(previous) + str(current)
			# 	featureDict[combinedEvent] += 1

		featureCount = []
		for f in sorted(featureDict.keys()):
			featureCount.append(featureDict[f])

		featureVectors.append(featureCount)
		labels.append(ex.label)

	return np.array(featureVectors), np.array(labels)