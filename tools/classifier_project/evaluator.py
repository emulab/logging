import argparse
import generation.loader as loader
from generation.featurizer import featurizer
from Models.SVM_model import *
from Models.Logistic_model import *
from Models.MLP_model import *
from Models.inv_model import *
from Models.Bayes_model import *
import csv

def evaluate(train, test, inv, clf, bigramF, fileName, trainP, testP):
	print("train:",train)
	print("test:",test)
	print("inv:",inv)
	print("CLF:",clf)
	print("Bigram:",bigramF)
	print("Out File:",fileName)
	print("Train partial:",trainP)
	print("Test partail:",testP)
	trainingData = loader.load(train, inv)
	testingData = loader.load(test, inv)
	#toFile("train.csv",trainingData)
	#toFile("test.csv",testingData)

	trainingFeatures = featurizer(trainingData)
	features, trainDf = trainingFeatures.loadData(bigram = bigramF, segmented = trainP)
	
	if clf == 'svm':
		SVM_1 = SVM()
		SVM_1.train(trainDf[features],trainDf['label'])
		testFeatures = featurizer(testingData)
		features, testDf = testFeatures.loadData(bigram = bigramF, segmented = testP)
		pred = SVM_1.test(testDf[features],testDf['label'])

	if clf == 'log':
		Log1 = Logistic()
		Log1.train(trainDf[features],trainDf['label'])
		testFeatures = featurizer(testingData)
		features, testDf = testFeatures.loadData(bigram = bigramF, segmented = testP)
		pred = Log1.test(testDf[features],testDf['label'])
	
	if clf == 'inv':
		invariantClf = invariant_clf()
		invariantClf.load_invariants(inv)
		testFeatures = featurizer(testingData)
		features, testDf = testFeatures.loadData(bigram = bigramF, segmented = testP)
		pred = invariantClf.eval_inv(testDf)

	if clf == 'mlp':
		MLP_clf = MLP()
		MLP_clf.train(trainDf[features],trainDf['label'])
		testFeatures = featurizer(testingData)
		features, testDf = testFeatures.loadData(bigram = bigramF, segmented = testP)
		pred = MLP_clf.test(testDf[features],testDf['label'])

	if clf == 'nb':
		NB_clf = NBayes()
		NB_clf.train(trainDf[features],trainDf['label'])
		testFeatures = featurizer(testingData)
		features, testDf = testFeatures.loadData(bigram = bigramF, segmented = testP)
		pred = NB_clf.test(testDf[features],testDf['label'])

	testDf['pred'] = pred
	print(testDf)

	if fileName:
		testDf.to_csv(fileName, index = None, header=True)

def toFile(filename, examples):
	header = ['Session ID','label','sequence']
	f = open(filename, mode = 'w')
	file_writer = csv.writer(f, delimiter=',')
	file_writer.writerow(header)

	for ex in examples:
		row = [ex.session_id, ex.label]
		for event in ex.sequence:
			row.append(event)
		file_writer.writerow(row)
	f.close()

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Model Evaluator')
	parser.add_argument('classifier', metavar='<clf>', type=str, help='Classifier selection: log, svm, inv, mlp, nb')
	parser.add_argument('train', metavar='<Train>', type=str, help='Training data csv')
	parser.add_argument('test', metavar='<Test>', type=str, help='Testing data csv')
	parser.add_argument('invariant', metavar='<Inv>', type=str, help='Invariants file')
	parser.add_argument("--bigram", help="Include bigram features", action="store_true")
	parser.add_argument("--f", type=str, help="Output results file w/ name")
	parser.add_argument("--trainP", help="Train of partial examples", action="store_true")
	parser.add_argument("--testP", help="Train of partial examples", action="store_true")


	args = parser.parse_args()
	print(args)
	classifers = ['log','svm','inv','mlp','nb']
	if args.classifier not in classifers:
		print(args.classifier + " is not a valid classifier selection.")
		print("Valid classifier options are:",classifers)
		exit()


	evaluate(args.train, args.test, args.invariant, args.classifier, args.bigram, args.f, args.trainP, args.testP)

