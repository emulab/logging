from generation.session_example import example
import numpy as np
import pandas as pd
class featurizer:
	def __init__(self, examples):
		self.examples = examples
		self.df = None
		self.features = None

	def loadData(self, segmented = False, bigram = False):
		print("Featurizing")
		# Collect all event_ids
		event_ids = []
		for ex in self.examples:
			for event in ex.sequence:
				if event not in event_ids:
					event_ids.append(str(event))
		event_ids = sorted(event_ids)

		blankFeatureDict = {}
		for event1 in event_ids:
			blankFeatureDict[event1] = 0
			if bigram:
				for event2 in event_ids:
					combinedEvent = event1 + event2
					blankFeatureDict[combinedEvent] = 0
		#print("Built blank")
		self.features = list(blankFeatureDict.keys())
		data = []
		#self.df = pd.DataFrame()
		df_list = []
		count = 0
		for ex in self.examples:
			featureDict = blankFeatureDict.copy()
			featureDict['label'] = ex.label
			featureDict['length'] = len(ex.sequence)
			featureDict['session'] = ex.session_id
			for i in range(0,len(ex.sequence)):
				current = ex.sequence[i]
				if i > 0:
					previous = ex.sequence[i-1]
				else:
					previous = None

				featureDict[current] += 1
				if bigram:
					if previous != None:
						combinedEvent = str(previous) + str(current)
						featureDict[combinedEvent] += 1
				if segmented:
					featureDict['segment'] = i + 1
					data.append(featureDict.copy())
					#self.df = self.df.append(featureDict, ignore_index=True)
			if not segmented:
				featureDict['segment'] = len(ex.sequence)
				data.append(featureDict.copy())
				#self.df = self.df.append(featureDict, ignore_index=True)
			#if len(self.df) % 100 == 0:
			#	print(len(self.df),"/",len(self.examples))
			#print(self.df)
				#data.append(featureDict)
			if len(data) > 10000:
				#print("Concat:",count,"+ ", len(data))
				count += len(data)
				temp_df = pd.DataFrame(data)
				#self.df = pd.concat([self.df, temp_df], ignore_index=True)
				df_list.append(temp_df)
				data =[]
		#self.df = pd.DataFrame(data)
		temp_df = pd.DataFrame(data)
		df_list.append(temp_df)
		print("Joining")
		self.df = pd.concat(df_list, ignore_index=True)
		#self.df = self.df.append(featureDict, ignore_index=True)
		print("Completed joining")
		return self.features, self.df

	def toFile(self, filename):
		header = ['Session ID','label','sequence']
		f = open(filename, mode = 'w')
		file_writer = csv.writer(f, delimiter=',')
		file_writer.writerow(header)

		for ex in self.examples:
			row = [ex.session_id, ex.label]
			for event in ex.sequence:
				row.append(event)
			file_writer.writerow(row)
		f.close()

		
