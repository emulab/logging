from os import listdir
from os.path import isfile, join
from generation import example_former as former
import csv
import re
import operator

def load(data, inv, label = True):
	print('loading')

	if '.csv' not in data:
		raise ValueError(data + 'is not a csv file.')

	# Load data from csv file
	csv_file = data
	entries = []
	with open(csv_file, "r") as f:
		reader = csv.reader(f, delimiter=",")
		for i, line in enumerate(reader):
			if i == 0:
				continue
			session_id = line[0].split('blk_')[1]
			date = line[0][0:15]
			event_id = line[1]
			source = line[3]
			offset = line[4]
			entry = [session_id, date, event_id, source, offset]
			entries.append(entry)

	examples = former.form(entries)

	if label:
		inv_file = inv

		f = open(inv_file, 'r') 
		contents = f.read()

		inv_list = []
		event_ids = re.findall(r'\((.*?)\)', contents)
		inv_counts = re.findall(r'\[(.*?)\]', contents)
		
		for i in range(0,len(event_ids)):
			event_id_list = event_ids[i].split(',')
			inv_counts_list = inv_counts[i].split(',')

			invariant = {}
			for j in range(0,len(event_id_list)):
				event_id = int(event_id_list[j].replace(' ',''))
				inv_count = float(inv_counts_list[j].replace(' ',''))
				invariant[event_id] = inv_count
			print(invariant)
			inv_list.append(invariant)

		total = len(examples)
		negatives = 0
		invariant_count = [0] * len(inv_list)
		invariant_dict = {}
		for ex in examples:
			invariant_check = [0] * len(inv_list)
			ex.label = 1
			for event in ex.sequence:
				for i in range(0,len(inv_list)):
					if int(event) in inv_list[i]:
						invariant_check[i] += inv_list[i][int(event)]
			
			for i in range(0,len(invariant_check)):
				if invariant_check[i] != 0:
					if ex.session_id not in invariant_dict:
						invariant_dict[ex.session_id] = []
					ex.label = -1
					negatives += 1
					invariant_count[i] += 1
					invariant_dict[ex.session_id].append(i)
					break
		print("Negative: " + str(negatives) +'/' + str(total))
	return examples


