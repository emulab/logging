import time
import datetime
from generation.session_example import example

def form(entries):
	sessions_dict = {}
	for entry in entries:
		session_id = entry[0]
		date = entry[1]			
		event_id = entry[2]
		source =entry[3]
		offset = entry[4]

		if session_id not in sessions_dict:
			sessions_dict[session_id] = {}

		date_obj = datetime.datetime.strptime(date, "%b %d %H:%M:%S").timetuple()

		if date_obj not in sessions_dict[session_id]:
			sessions_dict[session_id][date_obj] = {}

		sess_date = sessions_dict[session_id][date_obj]
		if source not in sess_date:
			sess_date[source] = {}
		if offset not in sess_date[source]:
			sess_date[source][offset] = []
		sess_date[source][offset].append(event_id)					

	examples = []
	for session in sessions_dict:
		seq = []
		for time in sorted(sessions_dict[session].keys()):
			for source in sorted(sessions_dict[session][time].keys()):
				for offset in sorted(sessions_dict[session][time][source].keys()):
					seq = seq + sessions_dict[session][time][source][offset]
		#print(session, " : ",seq)
		ex = example(session,seq)
		examples.append(ex)

	return examples