import json

class QueryBuilder:
	def __init__(self, nodes = [], 
		logs = [], 
		dateRange = [], 
		location = ['boss.utah.cloudlab.us']):
		self.nodes = nodes
		self.logs = logs
		self.dateRange = dateRange
		self.locations = []

	def build_query(self):
		node_locations = {
			"ms" : ["boss.utah.cloudlab.us"],
			"apt" : ["boss.apt.emulab.net"],
			"pc" : ["boss.emulab.net"],
			"hp" : ["boss.utah.cloudlab.us"],
			"clnode" : ["boss.clemson.cloudlab.us"],
			"c220g1" : ["boss.wisc.cloudlab.us"],
			"c220g2" : ["boss.wisc.cloudlab.us"],
			"c220g5" : ["boss.wisc.cloudlab.us"],
			"c240g5" : ["boss.wisc.cloudlab.us"],
			"pc-c220m4" : ["boss.wisc.cloudlab.us"],
			"pcwf" : ["boss.emulab.net"]
		}
		
		all_nodes = []
		for node in self.nodes:
			all_nodes.append(node.ID)
			self.locations = self.locations + node_locations[node.prefix]
		
		nodes_string = self.list_to_query_string('node_id',all_nodes)
		logs_string = self.list_to_query_string('source',self.logs)
		date_string = self.dateRange.date_list()
		loc_string = self.list_to_query_string('beat.hostname',self.locations)

		query_parts = [nodes_string, logs_string, date_string, loc_string]
		query_string = ''

		for part in query_parts:
			if part == '':
				continue
			if query_string == '':
				query_string = query_string + "(" + part + ")"
			else:
				query_string = query_string + " AND (" + part + ")"
		query = {
			"query": {
				"query_string": {
					"query": query_string
   				}
 			}
		}
		return query

	def list_to_query_string(self, val, list):
		query_string = ''
		for item in list:
			if val == 'node_id':
				item = '\"' + item +'\"'
			if query_string == '':
				query_string = query_string + val + ':' +  item
			else:
				query_string = query_string + " OR " + val + ':' + item
		return query_string

class nodeID:
	def __init__(self, prefix, ID):
		self.prefix = prefix
		self.ID = ID

class dateSet:
	def __init__(self, start_month, start_year, end_month, end_year):
		self.start_month = start_month
		self.start_year = start_year
		self.end_month = end_month
		self.end_year = end_year

	def date_list(self):
		months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		index = months.index(self.start_month)
		year = self.start_year

		query_string = ''
		month_list = []
		while(True):
			month_year = " (timestamp:" + months[index] + " AND @timestamp: [" + year +"-01-01 TO " + year+ "-12-31] ) " 
			if query_string == '':
				query_string = query_string + month_year
			else:
				query_string = query_string + " OR " + month_year
			if months[index] == self.end_month and year == self.end_year:
				break
			index += 1
			if (index == 12):
				index = 0
				year = str(int(year) + 1)
		return query_string
