import csv
import os
from EntryBuilder import *
from QueryBuilder import *
import git

class FileBuilder:
	def __init__(self, name, nodes, dates, logs, window, header = ['Content','EventId','EventTemplate','Source','Offset']):
		self.filename = name
		self.header = header

		self.nodes = nodes
		self.dates = dates
		self.logs = logs
		self.window = window

		# Set folder path to logdata directory or put in current directory
		self.folder_path = '/local/logdata/boot/logs/'
		if not os.path.exists(self.folder_path):
			self.folder_path = ''
		self.folder_path = self.folder_path + self.filename
		os.mkdir(self.folder_path)
		print('Created folder: ',self.folder_path)
		
		# Open csv file
		self.file = open(self.folder_path + '/' + self.filename + '.csv', mode = 'w')
		self.file_writer = csv.writer(self.file, delimiter=',')
		self.file_writer.writerow(header)

		self.create_data_description()

	def create_data_description(self):
		file = open(self.folder_path + '/' + self.filename + '.txt','w') 
 
		file.write('File description for: ' + self.filename + '\r\n')

		for node in self.nodes:
			file.write(node + '\r\n')

		file.write('\r\n')

		for log in self.logs:
			file.write(log + '\r\n')

		file.write('\r\n')

		file.write(self.dates.start_month + ' ' + self.dates.start_year +' - ' + self.dates.end_month + ' ' + self.dates.end_year + '\r\n')
 
		file.write(self.window + ' window' + '\r\n')

		repo = git.Repo(search_parent_directories=True)
		sha = repo.head.object.hexsha

		file.write('Hash: ' + str(sha))

		file.close() 

	def addEntries(self, entries):
		for entry in entries:
			fullString = entry.message
			fullString = fullString + ', '.join(entry.blockids)
			row = [fullString, entry.eventid, entry.message, entry.source, entry.offset]
			self.file_writer.writerow(row)

	def close(self):
		self.file.close()
