# DataParser

Tool for parsing data from Elasticsearch data into CSV files that can be used to
train the invariant miner or other ML models.

## Setup and Installation
All the necessary code to run the data parser is contained in this directory 
except some required python packages.

CloudLab machines may not have `pip3` installed, this can be done with:
```
sudo apt install python3-pip
```

The packages can then be installed with:
```
pip3 install -r requirements.txt
```

## Usage
The data parser is started by running:
```
python3 DataParser.py
```

The data parser will ask a series of questions to get the necessary information
to builda series of custom Elasticsearch queries.

The parser will ask for information on:
- Node type (prefix) ie. hp, ms, clnode, pc, apt
- Selected range for each node type
- Log types to include
- Start and end month/year for selected time period
- Window type for session IDs (fixed or sliding)
- Output file name

Example inputs:
```
python3 DataParser.py 
Node options:  ['hp', 'ms', 'apt', 'pc', 'clnode']
Enter desired node types, seperated by spaces: hp

Enter start of range for nodes hp: 1
Enter end of range for nodes hp: 10

Log options are below, type in numbers seperated by spaces or leave blank for all: 
0 bootinfo.log
1 stated.log
2 reboot.log
3 dhcpd.log
4 auth.log


Enter start month and year, ie. Jan 2019: Jan 2019
Enter end month and year, ie. Dec 2019: Jan 2019

Window options are below, type in a number to select: 
0 fixed
1 sliding

0

Enter output folder name: example_run

```

## Output
The data parser will create a folder with the specified name, defaulting to the 
`/local/logdata/boot/logs` directory if available or the current directory otherwise.

The folder will contain a CSV file with the data retrieved and a .txt file that
contains information on the query specifications.