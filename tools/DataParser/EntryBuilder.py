import json

class EntryBuilder:
	def __init__(self, IDtype):
		self.idType = IDtype

	def createEntries(self, source):
		entries = []
		for hit in source['hits']['hits']:
			entry = LogEntry(hit, self.idType)
			if entry.valid:
				entries.append(entry)
		#print(len(entries),"are valid out of",len(source['hits']['hits']))
		return entries


class LogEntry:
	def __init__(self, entry, IDtype):
		self.idType = IDtype
		self.raw = entry
		self.blockids = []

		log_entry = self.raw['_source']
		if not (log_entry.get('eventid')):
			self.valid = False
			return None
		self.eventid = log_entry['eventid']
		self.message = log_entry['message']
		self.nodeID = log_entry['node_id']
		self.offset = log_entry['offset']
		self.source = log_entry['source']
		if isinstance(log_entry['timestamp'], (list,)):
			self.timestamp = log_entry['timestamp'][0].replace('  ',' ')
		else:
			self.timestamp = log_entry['timestamp'].replace('  ',' ')
		date = self.timestamp.split(' ')
		self.hour = date[2].split(':')[0]
		self.day = date[1]
		if len(self.day) < 2:
			self.day = '0' + self.day
		self.month = date[0]
		at_date = log_entry['@timestamp'].replace("T","-").split('-')
		self.year = at_date[0]
		self.at_month = at_date[1]
		self.at_day = at_date[2]

		self.valid = self.validate_entry()
		self.blockids = self.generate_block_id()

	def validate_entry(self):
		# Deal with mismatched dates
		if int(self.month_to_num(self.month)) != int(self.at_month):
			month_days = {'Jan': 31, 'Feb': 28, 'Mar': 31, 'Apr': 30, 'May': 31, 'Jun': 30, 'Jul': 31, 'Aug': 31, 'Sep': 30, 'Oct': 31, 'Nov': 30, 'Dec': 31}
			# Correct year rollover
			if int(self.month_to_num(self.month)) == 12 and int(self.at_month) == 1 and int(self.day) == 31 and int(self.at_day) == 1:
				self.year = str(int(self.year) - 1)
				return False
			elif int(self.day) == int(month_days[self.month]) and int(self.at_day) == 1:
				return True
			else:
				return False
		return True
		

	def generate_block_id(self):
		if self.idType == 'fixed':
			return self._fixed_window()
		if self.idType == 'sliding':
			return self._sliding_window()
		raise Exception("Invalid Window Type")

	def _fixed_window(self):
		node_types = {'hp' : '01',
		'ms' : '02',
		'apt' : '03',
		'clnode' : '04',
		'pc-c220m4' : '06',
		'pcwf' : '07',
		'pc' : '05',
		'c220g1' : '08',
		'c220g2' : '09',
		'c220g5' : '10',
		'c240g5' : '11'
		}

		node_id = self.nodeID
		for prefix in node_types:
			if prefix in node_id:
				node_id = node_id.replace(prefix, node_types[prefix])
				node_id = ''.join([i for i in node_id.replace("pc-c220m4","") if i.isdigit()])
				break
		block_id = 'blk_' + self.year + self.month_to_num(self.month) + self.day + node_id
		return [block_id]

	def _sliding_window(self):
		months = ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		month_days = {'Jan': 31, 'Feb': 28, 'Mar': 31, 'Apr': 30, 'May': 31, 'Jun': 30, 'Jul': 31, 'Aug': 31, 'Sep': 30, 'Oct': 31, 'Nov': 30, 'Dec': 31}
		month = self.month
		day = self.day
		year = self.year
		hour = self.hour

		node_types = {'hp' : '01',
		'ms' : '02',
		'apt' : '03',
		'clnode' : '04',
		'pc-c220m4' : '06',
		'pcwf' : '07',
		'pc' : '05',
		'c220g1' : '08',
		'c220g2' : '09',
		'c220g5' : '10',
		'c240g5' : '11'
		}
		node_id = self.nodeID
		for prefix in node_types:
			if prefix in node_id:
				node_id = node_id.replace(prefix, node_types[prefix])
				node_id = ''.join([i for i in node_id.replace("pc-c220m4","") if i.isdigit()])
				break

		block_ids = []
		for i in range(0,24):
			if len(hour) < 2:
				hour = '0' + hour
			if len(day) < 2:
				day = '0' + day

			block_id = 'blk_' + year + self.month_to_num(month) + day + hour + node_id
			block_ids.append(block_id)
			
			hour = str(int(hour) - 1)
			if int(hour) < 0:
				hour = '23'
				day = str(int(day) - 1)
				if int(day) == 0:
					month = months[months.index(month) - 1]
					day = str(month_days[month])
					if month == 'Dec':
						year = str(int(year) - 1)
		return block_ids


	def month_to_num(self, month):
		months = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}
		return months[month]


