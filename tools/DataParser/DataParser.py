from ElasticClient import ElasticClient as es
from QueryBuilder import *
from getpass import getpass
from EntryBuilder import *
from FileBuilder import *
import os

class DataParser:
	def __init__(self):
		self.sources = []
		self.nodes = []
		self.dates = None
		self.size = 1000
		self.scroll = False
		self.window = ''
		self.logs = []
		self.username = ''
		self.password = ''
		self.filename = ''

	def ParseData(self):
		entryBuilder = EntryBuilder(self.window)

		fb = FileBuilder(self.filename, self.nodes, self.dates, self.logs, self.window)
		client = es(self.user, self.password)
		for node_type in self.nodes:
			nodes = []
			filename = "node_lists/" + node_type + '.txt'

			with open(filename, "r") as f:
				for line in f:
					nodes.append(line.replace("\n",""))

			for node in nodes:
				print(node)
				node_id = nodeID(node_type, node)
				qb = QueryBuilder(nodes = [node_id], logs = self.logs, dateRange=self.dates)
				query = qb.build_query()
				results = client.send_query(query, scroll = self.scroll, size = self.size)
				entries = entryBuilder.createEntries(results)
				fb.addEntries(entries)

				if (self.scroll):
					size = len(results['hits']['hits'])
					while (size > 0):
						size, page = client.scroll()
						if size == 0:
							break
						entries = entryBuilder.createEntries(page)
						fb.addEntries(entries)
		fb.close()

valid_nodes = ['hp',
		'ms',
		'apt',
		'clnode',
		'pc-c220m4',
		'pcwf',
		'pc',
		'c220g1',
		'c220g2',
		'c220g5',
		'c240g5' ]
id_lens = {'hp' : 3, 'ms' : 4, 'apt' : 3, 'pc' : 3, 'clnode': 3}
valid_logs = ['bootinfo.log', 'stated.log', 'reboot.log', 'dhcpd.log', 'auth.log']
valid_windows = ['fixed','sliding']

parser = DataParser()


# Request node type information
while(True):
	print("Node options: ",valid_nodes)
	nodes_types = input("Enter desired node types, seperated by spaces: ")
	nodes = nodes_types.split(' ')
	valid = True

	if nodes_types == '':
		print("No nodes selected, valid options are: ")
		print(valid_nodes)
		continue

	for node in nodes:
		if node not in valid_nodes:
			print("Invalid node: ", node)
			valid = False
	
	parser.nodes = nodes
	if valid:
		break

# Requests the user select the types of logs to retrieve
# while(True):
# 	print('')
# 	print("Log options are below, type in numbers seperated by spaces or leave blank for all: ")
# 	for i in range(0,len(valid_logs)):
# 		print(i,valid_logs[i])
# 	print()
# 	user_input = input()
# 	if user_input == '':
# 		parser.logs = valid_logs
# 		break
# 	selections = user_input.split(' ')
# 	logs = []
# 	valid = True
# 	for select in selections:
# 		try:
#    			logs.append(valid_logs[int(select)])
# 		except Exception:
# 			valid = False
# 			print(select, " is not a valid choice.")
# 			break
# 	if valid:
# 		parser.logs =  logs
# 		break
parser.logs = ['bootinfo.log', 'stated.log', 'reboot.log', 'dhcpd.log']

# Get start month for query
months = ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
years = ['2018','2019']
while(True):
	start = input("Enter start month and year, ie. Jan 2019: ")
	date_entered = start.split(' ')
	if len(date_entered) != 2:
		print("invalid format, correct format is MONTH YEAR")
		continue
	start_month = date_entered[0]
	start_year = date_entered[1]
	if start_month not in months:
		print(start_month, "is not a valid month.")
		continue
	if start_year not in years:
		print(start_year, "is not a valid year.")
		print("Year options are: ",years)
		continue
	break

# Get end month for query
while(True):
	end = input("Enter end month and year, ie. Dec 2019: ")
	date_entered = end.split(' ')
	if len(date_entered) != 2:
		print("invalid format, correct format is MONTH YEAR")
		continue
	end_month = date_entered[0]
	end_year = date_entered[1]
	if end_month not in months:
		print(end_month, "is not a valid month.")
		continue
	if end_year not in years:
		print(end_year, "is not a valid year.")
		print("Year options are: ",years)
		continue
	break
parser.dates= dateSet(start_month,start_year,end_month,end_year)

# Ask for specified query size
#print('')
#while(True):
#	user_input = input("Enter query size: ")
#	try:
 #  		parser.size = int(user_input)
#	except ValueError:
 #  		print(user_input, " is not an integer.")
  # 		continue
	#break
parser.size = 1000

# Request window options
print('')
while(True):
	print("Window options are below, type in a number to select: ")
	for i in range(0,len(valid_windows)):
		print(i,valid_windows[i])
	print()
	user_input = input()

	try:
   		parser.window = valid_windows[int(user_input)]
   		break
	except Exception:
		print(user_input, " is not a valid choice.")

print('')
parser.filename = input("Enter output folder name: ")

# Scrolling enablaing
#print('')
#while(True):
#	user_input = input("Would you like to enable scrolling (y/n): ")
#	if user_input == 'y' or user_input == 'Y':
#		parser.scroll = True
#		break
#	if user_input == 'n' or user_input == 'N':
#		break
parser.scroll = True

# Check for kibana credentials or request them
if os.path.exists('/local/logdata/kibana_cred.txt'):
	with open ('/local/logdata/kibana_cred.txt') as reader:
		parser.user = (str(reader.readline())).replace('\n','')
		parser.password = (str(reader.readline())).replace('\n','')
elif os.path.exists('kibana_cred.txt'):
	with open ('kibana_cred.txt') as reader:
		parser.user = (str(reader.readline())).replace('\n','')
		parser.password = (str(reader.readline())).replace('\n','')

else:
	print('')
	parser.user = input("Enter username: ")
	parser.password = getpass("Password: ")

parser.ParseData()
exit()