import ast
import sys

with open(sys.argv[1], "r") as data:
    dict1 = ast.literal_eval(data.read())
with open(sys.argv[2], "r") as data:
    dict2 = ast.literal_eval(data.read())    
def intersect(a, b):
    print ('matching:',{k: a[k] for k in a if b.get(k, not a[k]) == a[k]})
    print ('different:',{k : b[k] for k in set(b) - set(a) })
intersect(dict1,dict2)