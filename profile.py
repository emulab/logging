"""This profile is used to run experiments on CloudLab and Emulab's log data

Instructions:

This repository gets cloned in `/local/repository/`. The profile is set up to
put other code and data in `/local/logdata/`.

By default, the profile will clone logpai's loglizer code, and apply our
patches to it, in `/local/logdata/loglizer`

To grab the (private) datasets we are using, run `/local/repository/grabdata.sh`.
Note that ths will require your `ssh` keys, so make sure you have agent forwarding
enabled.

If you need to commit anything to this repository from within an experiment (or want
to update with `git pull`), you'll need to do the following first to make sure the
branch state is set up correctly:
    `cd /local/repository/ && git checkout master`
Also be sure that you have used `git config` to set up your name and email address.

"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab

# Create a portal context.
pc = portal.Context()

request = pc.makeRequestRSpec()
node = request.RawPC("node")

node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"

# Make some scratch space locally to hold log data we might download
bs = node.Blockstore("bs","/local/logdata")
bs.size = "30"

# Install some useful stuff
node.addService(rspec.Execute(shell="sh", command="/local/repository/setup.sh"))
node.addService(rspec.Execute(shell="sh", command="/local/repository/applypatch.sh"))

pc.printRequestRSpec(request)
