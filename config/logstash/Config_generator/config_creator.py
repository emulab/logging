import csv
import re
import hashlib
import os

def load_template():
	with open('Templates/config_template.txt', 'r') as content_file:
		template = content_file.read()
	return template

def read_spec(filename):
	patterns = []
	print(filename)
	log_name = ""
	version = 0
	with open(filename, 'r') as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		count = 0
		for row in reader:
			if(count == 0):
				log_name = row[0]
				version = row[1]
			else:
				patterns.append(row[0])
			count = count + 1
	return log_name, version, patterns

def pattern_overwrites(patterns):
	overwrites = []
	for pattern in patterns:
		overwrite = []
		extract = re.findall(r'\{(.*?)\}', pattern)
		for entry in extract:
			entry_split = entry.split(':')
			if (len(entry_split) > 1):
				overwrite.append(entry_split[1])
		overwrites.append(overwrite)
	return overwrites

def build_grok_entries(log, patterns):
	with open('Templates/pattern_format.txt', 'r') as content_file:
		pattern_format = content_file.read()
	middle_content = ''
	overwrites = pattern_overwrites(patterns)
	myfile = open('elasticsearch_hashes.csv', mode = 'a')
	file_writer = csv.writer(myfile, delimiter=',')
	count = 0
	for pattern in patterns:
		grok_entry = pattern_format.replace('PATTERN',pattern)
		overwrite_string = ''
		
		start = 0
		for overwrite in overwrites[count]:
			if start != 0:
				overwrite_string += ', '
			overwrite_string += "\""
			overwrite_string += overwrite
			overwrite_string += "\""
			start = start + 1
		
		grok_entry = grok_entry.replace("OVERWRITES",overwrite_string)

		hash = int(hashlib.sha1(pattern.encode()).hexdigest(), 16) % (10 **8 )
		file_writer.writerow([str(hash), pattern])

		grok_entry = grok_entry.replace('HASH',"\"" + str(hash) + "\"")
		middle_content = middle_content + grok_entry
		count = count + 1
	myfile.close()
	return middle_content

def build_queries(log_files, versions):
	with open('Templates/query_template.txt', 'r') as content_file:
		query_template = content_file.read()
	queries = ""
	i = 0;
	for log in log_files:
		if (i != 0):
			queries += ','
		queries += query_template.replace("LOG NAME",log).replace("VERSION NUMBER",versions[i])
		i = i +1
	return queries

def load_specs():
	log_files = os.listdir("Specs")
	log_names = []
	patterns_list = []
	versions = []
	for log in log_files:
		if (".csv" not in log):
			continue
		log_name, verion, patterns = read_spec("Specs/" + log)
		log_names.append(log_name)
		patterns_list.append(patterns)
		versions.append(verion)
	return log_names, versions, patterns_list

def build_log_groks(log_names, patterns_list, version_list):
	with open('Templates/log_template.txt', 'r') as content_file:
		log_template = content_file.read()

	# To remove file to prevent appending to old hashes
	myfile = open('elasticsearch_hashes.csv', mode = 'w')
	myfile.close()
	
	log_groks = ""
	i = 0
	
	for log in log_names:
		grok_entries = ''
		source = "/" + log.replace(".","\.") + "/"
		grok_entries += build_grok_entries(log, patterns_list[i])
		log_groks += log_template.replace("SOURCE",source).replace("## GROK PATTERNS ##", grok_entries).replace("VERSION NUMBER", version_list[i])
		i = i + 1
	return log_groks

log_names, version_list, patterns_list = load_specs()
queries = build_queries(log_names, version_list)
log_groks = build_log_groks(log_names, patterns_list, version_list)
config_template = load_template()
config_template = config_template.replace("## LOG QUERIES ##",queries).replace("## GROK ENTRIES ##",log_groks)

f = open("logstash.emulab.net.conf", "w")
f.write(config_template)
f.close()