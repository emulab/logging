input {
    beats {
        port => 5044

        # Set to False if you do not SSL
        ssl => true

        # Delete below lines if no SSL is used
        ssl_certificate => "/var/easy-rsa/keys/logstash.emulab.net.crt"
        ssl_key => "/var/easy-rsa/keys/logstash.emulab.net.key"
        ssl_certificate_authorities => "/var/easy-rsa/keys/ca.crt"
        ssl_verify_mode => "force_peer"
    }

    elasticsearch {
        hosts => "localhost"
        index => "*"
        docinfo => true
        size => 1000
        scroll => "5m"
        schedule => "* * * * *"
        query => '{"query": {"bool" : {"should": [ 
{"bool": {
     "must_not": { "match" : {"eventid_filter_version" : "2"}},
     "must" : {"match" : {"source" : "reboot.log" }}
    }}
,
{"bool": {
     "must_not": { "match" : {"eventid_filter_version" : "2"}},
     "must" : {"match" : {"source" : "auth.log" }}
    }}
,
{"bool": {
     "must_not": { "match" : {"eventid_filter_version" : "1"}},
     "must" : {"match" : {"source" : "dhcpd.log" }}
    }}
,
{"bool": {
     "must_not": { "match" : {"eventid_filter_version" : "1"}},
     "must" : {"match" : {"source" : "bootinfo.log" }}
    }}
,
{"bool": {
     "must_not": { "match" : {"eventid_filter_version" : "2"}},
     "must" : {"match" : {"source" : "stated.log" }}
    }}
]}}}'
    }
}

filter {


  if [source] =~ /reboot\.log/ {
      mutate {
          replace => { "eventid_filter_version" => 2 }
          remove_field => [ "eventid" ]
      }

         if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} BATCH: rebooting %{GREEDYDATA:node_ids}" }
            overwrite => [ "timestamp", "node_ids" ]
            add_field => { "eventid" => "59630397" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: trying ssh reboot" }
            overwrite => [ "timestamp", "node_id" ]
            add_field => { "eventid" => "51500045" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: ssh reboot returned %{INT:reboot_ssh_return}" }
            overwrite => [ "timestamp", "node_id", "reboot_ssh_return" ]
            add_field => { "eventid" => "1107732" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: waiting 30s for reboot" }
            overwrite => [ "timestamp", "node_id" ]
            add_field => { "eventid" => "93637279" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: rebooted" }
            overwrite => [ "timestamp", "node_id" ]
            add_field => { "eventid" => "42965520" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: appears dead, power cycle" }
            overwrite => [ "timestamp", "node_id" ]
            add_field => { "eventid" => "17595896" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: ssh reboot failed, sending ipod" }
            overwrite => [ "timestamp", "node_id" ]
            add_field => { "eventid" => "29489217" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} BATCH: power cycling %{GREEDYDATA:node_ids}" }
            overwrite => [ "timestamp", "node_ids" ]
            add_field => { "eventid" => "10853808" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: ipod failed, power cycling" }
            overwrite => [ "timestamp", "node_id" ]
            add_field => { "eventid" => "66447921" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} /usr/testbed/bin/node_reboot -r %{USERNAME:node_id}" }
            overwrite => [ "timestamp", "node_id" ]
            add_field => { "eventid" => "90531542" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: in %{WORD:reboot_state}, sending %{WORD:reboot_message}" }
            overwrite => [ "timestamp", "node_id", "reboot_state", "reboot_message" ]
            add_field => { "eventid" => "33079393" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: waiting %{INT:reboot_wait_time}s for ipod" }
            overwrite => [ "timestamp", "node_id", "reboot_wait_time" ]
            add_field => { "eventid" => "91628661" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: ssh reboot hung, sending ipod" }
            overwrite => [ "timestamp", "node_id" ]
            add_field => { "eventid" => "75482038" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: in %{WORD:reboot_state}, but power cycling in killmode" }
            overwrite => [ "timestamp", "node_id", "reboot_state" ]
            add_field => { "eventid" => "8482523" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id}: powered off, will power on" }
            overwrite => [ "timestamp", "node_id" ]
            add_field => { "eventid" => "50016557" }
        }
}


  }
  if [source] =~ /auth\.log/ {
      mutate {
          replace => { "eventid_filter_version" => 2 }
          remove_field => [ "eventid" ]
      }

         if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id} %{WORD:sshd_hostname} sshd\[%{INT:pid}\]: Received disconnect from %{IP:sshd_remote_ip} port %{INT:sshd_remote_port}:%{INT:sshd_code}: Normal Shutdown, Thank you for playing \[%{WORD:sshd_disconnect_stage}\]" }
            overwrite => [ "timestamp", "node_id", "sshd_hostname", "pid", "sshd_remote_ip", "sshd_remote_port", "sshd_code", "sshd_disconnect_stage" ]
            add_field => { "eventid" => "10114508" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id} %{WORD:sshd_hostname} sshd\[%{INT:pid}\]: Disconnected from authenticating user %{WORD:sshd_username} %{IP:sshd_remote_ip} port %{INT:sshd_remote_port} \[%{WORD:sshd_disconnect_stage}\]" }
            overwrite => [ "timestamp", "node_id", "sshd_hostname", "pid", "sshd_username", "sshd_remote_ip", "sshd_remote_port", "sshd_disconnect_stage" ]
            add_field => { "eventid" => "89934706" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id} %{WORD:sshd_hostname} sshd\[%{INT:pid}\]: Disconnected from %{IP:sshd_remote_ip} port %{INT:sshd_remote_port} \[%{WORD:sshd_disconnect_stage}\]" }
            overwrite => [ "timestamp", "node_id", "sshd_hostname", "pid", "sshd_remote_ip", "sshd_remote_port", "sshd_disconnect_stage" ]
            add_field => { "eventid" => "88049147" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id} %{WORD:sshd_hostname} sshd\[%{INT:pid}\]: Received disconnect from %{IP:sshd_remote_ip} port %{INT:sshd_remote_port}:%{INT:sshd_code}: Bye Bye \[%{WORD:sshd_disconnect_stage}\]" }
            overwrite => [ "timestamp", "node_id", "sshd_hostname", "pid", "sshd_remote_ip", "sshd_remote_port", "sshd_code", "sshd_disconnect_stage" ]
            add_field => { "eventid" => "54923798" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id} %{WORD:sshd_hostname} sshd\[%{INT:pid}\]: Invalid user %{WORD:sshd_username} from %{IP:sshd_remote_ip} port %{INT:sshd_remote_port}" }
            overwrite => [ "timestamp", "node_id", "sshd_hostname", "pid", "sshd_username", "sshd_remote_ip", "sshd_remote_port" ]
            add_field => { "eventid" => "23805815" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id} %{WORD:sshd_hostname} sshd\[%{INT:pid}\]: Invalid user %{WORD:sshd_username} from %{IP:sshd_remote_ip}" }
            overwrite => [ "timestamp", "node_id", "sshd_hostname", "pid", "sshd_username", "sshd_remote_ip" ]
            add_field => { "eventid" => "31311614" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id} %{WORD:sshd_hostname} sshd\[%{INT:pid}\]: input_userauth_request: invalid user %{WORD:sshd_username} \[%{WORD:sshd_disconnect_stage}\]" }
            overwrite => [ "timestamp", "node_id", "sshd_hostname", "pid", "sshd_username", "sshd_disconnect_stage" ]
            add_field => { "eventid" => "45436128" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id} %{WORD:sshd_hostname} sshd\[%{INT:pid}\]: Received disconnect from %{IP:sshd_remote_ip} port %{INT:sshd_remote_port}:%{INT:sshd_code}: disconnected by user" }
            overwrite => [ "timestamp", "node_id", "sshd_hostname", "pid", "sshd_remote_ip", "sshd_remote_port", "sshd_code" ]
            add_field => { "eventid" => "66302503" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} %{USERNAME:node_id} %{WORD:sshd_hostname} sshd\[%{INT:pid}\]: Disconnecting: Too many authentication failures \[%{WORD:sshd_disconnect_stage}\]" }
            overwrite => [ "timestamp", "node_id", "sshd_hostname", "pid", "sshd_disconnect_stage" ]
            add_field => { "eventid" => "39909593" }
        }
}


  }
  if [source] =~ /dhcpd\.log/ {
      mutate {
          replace => { "eventid_filter_version" => 1 }
          remove_field => [ "eventid" ]
      }

         if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGBASE2} %{WORD:dhcp_action} from %{MAC:node_mac} via %{IP:dhcp_server}" }
            overwrite => [ "dhcp_action", "node_mac", "dhcp_server" ]
            add_field => { "eventid" => "66787491" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGBASE2} %{WORD:dhcp_action} on %{IP:node_ip} to %{MAC:node_mac} via %{IP:dhcp_server}" }
            overwrite => [ "dhcp_action", "node_ip", "node_mac", "dhcp_server" ]
            add_field => { "eventid" => "22318753" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGBASE2} %{NONNEGINT:dhcp_badchecksums} bad %{WORD:dhcp_badproto} checksums in %{NONNEGINT:dhcp_badpackets} packets" }
            overwrite => [ "dhcp_badchecksums", "dhcp_badproto", "dhcp_badpackets" ]
            add_field => { "eventid" => "20475278" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGBASE2} %{WORD:dhcp_execute_statement} argv\[%{INT:dhcp_execute_argnumber}\]  %{GREEDYDATA:dhcp_execute_argument}" }
            overwrite => [ "dhcp_execute_statement", "dhcp_execute_argnumber", "dhcp_execute_argument" ]
            add_field => { "eventid" => "56028461" }
        }
}


  }
  if [source] =~ /bootinfo\.log/ {
      mutate {
          replace => { "eventid_filter_version" => 1 }
          remove_field => [ "eventid" ]
      }

         if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGBASE} %{IP:node_ip}: %{WORD:bootinfo_operation} \(vers %{INT:bootinfo_version}\)" }
            overwrite => [ "node_ip", "bootinfo_operation", "bootinfo_version" ]
            add_field => { "eventid" => "39947749" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGBASE} %{IP:node_ip}: %{WORD:bootinfo_operation}\(%{INT:bootinfo_event}\): %{GREEDYDATA:bootinfo_event_argument}" }
            overwrite => [ "node_ip", "bootinfo_operation", "bootinfo_event", "bootinfo_event_argument" ]
            add_field => { "eventid" => "28867222" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGBASE} %{IP:node_ip}: no event will be sent: last:%{INT:bootinfo_last_ts} cur:%{INT:bootinfo_cur_ts}" }
            overwrite => [ "node_ip", "bootinfo_last_ts", "bootinfo_cur_ts" ]
            add_field => { "eventid" => "28635907" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGBASE} %{IP:node_ip}: %{WORD:bootinfo_operation}: query bootinfo" }
            overwrite => [ "node_ip", "bootinfo_operation" ]
            add_field => { "eventid" => "10213379" }
        }
}


  }
  if [source] =~ /stated\.log/ {
      mutate {
          replace => { "eventid_filter_version" => 2 }
          remove_field => [ "eventid" ]
      }

         if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: Checking for mode transition" }
            overwrite => [ "timestamp", "pid", "node_id" ]
            add_field => { "eventid" => "69498507" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: CtrlEvent: %{WORD:stated_ctrlevent}" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_ctrlevent" ]
            add_field => { "eventid" => "30222656" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: RESET done, bootwhat returns %{WORD:stated_bootwhat_opmode},%{INT:stated_bootwhat_return}" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_bootwhat_opmode", "stated_bootwhat_return" ]
            add_field => { "eventid" => "22666147" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: Sending apod" }
            overwrite => [ "timestamp", "pid", "node_id" ]
            add_field => { "eventid" => "65423146" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: BootWhat says %{INT:stated_bootwhat_return} \(mode %{WORD:stated_bootwhat_opmode}\)" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_bootwhat_return", "stated_bootwhat_opmode" ]
            add_field => { "eventid" => "14430487" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: Invalid transition for node %{USERNAME:node_id} from %{WORD:stated_prev_opmode}\/%{WORD:stated_prev_state} to %{WORD:stated_next_state}" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_prev_opmode", "stated_prev_state", "stated_next_state" ]
            add_field => { "eventid" => "55838365" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: Mode change %{WORD:stated_prev_opmode} => %{WORD:stated_next_opmode} %{WORD:stated_forced}" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_prev_opmode", "stated_next_opmode", "stated_forced" ]
            add_field => { "eventid" => "66279350" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{WORD:stated_head}: %{USERNAME:node_id} in %{INT:stated_next_time}, queue=%{INT:stated_queue_length}" }
            overwrite => [ "timestamp", "pid", "stated_head", "node_id", "stated_next_time", "stated_queue_length" ]
            add_field => { "eventid" => "3522196" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: OBJTYPE=\'%{WORD:stated_objtype}\', OBJNAME=\'%{USERNAME:node_id}\', EVENTTYPE=\'%{WORD:stated_eventype}\'" }
            overwrite => [ "timestamp", "pid", "stated_objtype", "node_id", "stated_eventype" ]
            add_field => { "eventid" => "88207394" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: Node %{USERNAME:node_id} has timed out in state %{WORD:stated_prev_opmode}\/%{WORD:stated_prev_state} - %{WORD:stated_timeout_action} requested" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_prev_opmode", "stated_prev_state", "stated_timeout_action" ]
            add_field => { "eventid" => "54449269" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: Node %{USERNAME:node_id} has timed out in state %{WORD:stated_prev_opmode}\/%{WORD:stated_prev_state}" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_prev_opmode", "stated_prev_state" ]
            add_field => { "eventid" => "699977" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: Node %{USERNAME:node_id} has timed out too many times!" }
            overwrite => [ "timestamp", "pid", "node_id" ]
            add_field => { "eventid" => "38244800" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: %{WORD:stated_prev_opmode}/%{WORD:stated_prev_mode} => %{WORD:stated_next_opmode}/%{WORD:stated_next_state}" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_prev_opmode", "stated_prev_mode", "stated_next_opmode", "stated_next_state" ]
            add_field => { "eventid" => "9237422" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{WORD:stated_head}: %{USERNAME:node_id} in %{INT:stated_next_time}, queue=%{INT:stated_queue_length}" }
            overwrite => [ "timestamp", "pid", "stated_head", "node_id", "stated_next_time", "stated_queue_length" ]
            add_field => { "eventid" => "3522196" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{WORD:stated_queue_operation}: %{USERNAME:node_id} in %{INT:stated_next_time}, queue=%{INT:stated_queue_length}" }
            overwrite => [ "timestamp", "pid", "stated_queue_operation", "node_id", "stated_next_time", "stated_queue_length" ]
            add_field => { "eventid" => "86254722" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: Ignored spurious %{WORD:stated_transition} transition" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_transition" ]
            add_field => { "eventid" => "95899069" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: Clearing reload info" }
            overwrite => [ "timestamp", "pid", "node_id" ]
            add_field => { "eventid" => "48255014" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: Released from \[Experiment: %{WORD:emulab_pid}\/%{WORD:emulab_eid}\]" }
            overwrite => [ "timestamp", "pid", "node_id", "emulab_pid", "emulab_eid" ]
            add_field => { "eventid" => "24821205" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: %{WORD:stated_operation} %{WORD:stated_success}" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_operation", "stated_success" ]
            add_field => { "eventid" => "12913779" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: Rebooting nodes: %{GREEDYDATA:node_ids}" }
            overwrite => [ "timestamp", "pid", "node_ids" ]
            add_field => { "eventid" => "107033" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: Performed %{WORD:stated_operation} for %{USERNAME:node_id}" }
            overwrite => [ "timestamp", "pid", "stated_operation", "node_id" ]
            add_field => { "eventid" => "66328917" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: Command: %{USERNAME:node_id}, %{WORD:stated_operation} \(attempt %{INT:stated_attempt}\)" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_operation", "stated_attempt" ]
            add_field => { "eventid" => "86041760" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: Invalid mode transition for %{USERNAME:node_id} from %{WORD:stated_prev_opmode}\/%{WORD:stated_prev_state} to %{WORD:stated_next_opmode}" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_prev_opmode", "stated_prev_state", "stated_next_opmode" ]
            add_field => { "eventid" => "47837969" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: %{USERNAME:node_id}: Command %{WORD:stated_operation}, retry #%{INT:stated_attempt}" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_operation", "stated_attempt" ]
            add_field => { "eventid" => "72013458" }
        }
}
if ![eventid] {
grok {
            match => { "message" => "%{SYSLOGTIMESTAMP:timestamp} \[%{INT:pid}\]: Node %{USERNAME:node_id} timed out %{INT:stated_number_of_times} times in %{WORD:stated_prev_state}" }
            overwrite => [ "timestamp", "pid", "node_id", "stated_number_of_times", "stated_prev_state" ]
            add_field => { "eventid" => "40843288" }
        }
}


  }

    #
    # Translate node MAC addresses and IP addresses
    #   Right now, this is about "promoting" MACs and IP addresses to node_ids
    #   - should we try going the other direction too?
    #
    if [node_mac] and ![node_id] {
        translate {
            field => "node_mac"
            destination => "node_id"
            exact => true
            regex => false
            dictionary_path => "/etc/logstash/mac_to_id.csv"
            refresh_behaviour => "replace"
        }
    }

    #
    # Translate node IP addresses and node_ids
    #
    if [node_ip] and ![node_id] {
        translate {
            field => "node_ip"
            destination => "node_id"
            exact => true
            regex => false
            dictionary_path => "/etc/logstash/ip_to_id.csv"
            refresh_behaviour => "replace"
        }
    }


    if [type] == "syslog" {
        #grok {
        #    match => { "message" => "%{SYSLOGLINE}" }
        #}

        date {
            match => [ "timestamp", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
        }
    }
}

output {
    # Figure out if we are updating an existing entry or inserting a new one
    if  [@metadata][_id] {
        elasticsearch {
            hosts => localhost
            action => "update"
            index => "%{[@metadata][_index]}"
            document_id => "%{[@metadata][_id]}"
            doc_as_upsert => true
        }
    } else {
        elasticsearch {
            hosts => localhost
            index => "%{[@metadata][beat]}-%{+YYYY.MM.dd}"
        }
    }
    #stdout {
    #codec => rubydebug
    #   }
}

# vim: set ft=conf:
