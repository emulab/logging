
## System Configurations for elasticsearch
These configurations are necessary to ensure proper elasticsearch function

See https://www.elastic.co/guide/en/elasticsearch/reference/current/important-settings.html for additional information

## Set heap size
The heap size needs to be approxiametly 50% of system memory and the min and max must be set to the same number
* `sudo nano /etc/elasticsearch/jvm.options`
* Set `-Xms26g` ie. 26 GB
* Set `-Xmx26g`


## Disable swapping
In `elasticsearch/yml`, uncomment line `bootstrap.memory_lock: true`

## Increase file descirptor limit
In `/etc/security/limits.conf`:
* Add `elasticsearch  -  nofile  65536`
To make this persist on reboot access `/etc/pam.d/su` and uncomment:
* `# session    required   pam_limits.so`

## Increase size of virtual memory
In `/etc/sysctl.conf` set the below line:
* `vm.max_map_count=262144`

## Raise number of threads
 In `/etc/security/limits.conf` set the below line:
 * `nproc=4096`